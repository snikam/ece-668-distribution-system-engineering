clc
clear all
close all
S = [700 1000 500 1000 1000 1000];    % in kVa
pf = [0.8 0.82 0.85 0.8 0.85 0.8];
loading = [0.8 0.85 0.7 0.9 0.85 0.8];
Vold = [13.8 13.8 13.8 13.8 13.8 13.8];  % in kV

Z_permile = [0.10+0.25i 0.10+0.25i 0.12+0.35i 0.12+0.35i 0.15+0.45i 0.15+0.45i 0.10+0.25i]; % in ohm/mile

Length = [2 3 2.5 4 1.5 3 2]; % in mile
Distance = [2 5 7.5 11.5 13 16];



Z = Z_permile.*Length;
S = S.*loading;
P = S.*pf;
Q = sqrt((S.*S)-(P.*P));



Sbase = 1000; %kVA
Vbase = 13.8; %kV
Zbase = 13800*13800/1000000;
Vregulated = 1;  %kV
P = P./Sbase;
Q = Q./Sbase;
Vold = Vold./Vbase;
Vnew = ones(1,6);
Z = Z./Zbase;
Iline = zeros(1,7);
S = complex(P,Q);
%backward sweep
Iload = conj(S./Vnew);

for i=6:-1:1
    Iline(i) = Iload(i) + Iline(i+1);
end

% Forward Sweep
for i=1:6
    if i==1
        Vnew(i) = Vregulated - Iline(i).*Z(i);
    else 
        Vnew(i) = Vnew(i-1) - Iline(i).*Z(i);
    end
end
fprintf("V for first Iteration \n"); disp(abs(Vnew))
disp(abs(Vnew)*13.8)


DeltaV =1000;
Vold=Vnew;
V_tolerance = 0.01; % in V
V_tolerance = V_tolerance / (Vbase*1000);
%-----------------------Stopping Criterion-------------------------------
Count = 0;
while max(abs(DeltaV)) >= V_tolerance 
    % Backward Sweep
    Iload = conj(S(:)./Vnew(:));
    for i=6:-1:1
        Iline(i) = Iload(i) + Iline(i+1);
    end
    
    % Forward Sweep
    for i=1:6
        if i==1
            Vnew(i) = Vregulated - Iline(i).*Z(i);
        else 
            Vnew(i) = Vnew(i-1) - Iline(i).*Z(i);
        end
    end
    DeltaV = abs(Vold(:) - Vnew(:));
    Vold(:) = Vnew(:);
    Count = Count + 1;
    
end
disp(Count);
fprintf("The voltages after applying the stopping criterion \n")
disp(abs(Vnew));
disp(abs(Vnew)*13.8);
subplot(2,2,1);
plot(Distance,abs(Vnew));
grid on
xlabel('Distance')
ylabel('Voltage')
title('Voltage vs Distance')
%---------------------------Tap Setting----------------------------------

N = 13.8/0.48; %turns ratio
V_hv = Vnew*Vbase*1000;
Zint = [0.05 0.0575 0.0475 0.0575 0.0575 0.0575];
V_lv = V_hv./N ;
V_lv = V_lv-(V_lv.*Zint.*loading);
V_lv_beforetap = V_lv;

V_drop = zeros(1,6);

Tap = (abs(V_hv) - 13800)./(13800);
Tap = Tap.*100;
fprintf("The intial Tap settings calulated are \n"); disp(Tap)
% apply -2.5% tap at T1, T2 and -5% tap at T3, T4, T5, T6

V_lv_aftertap = zeros(1,6);
V_lv_aftertap1 = zeros(1,6);


for i = 1:2
    
    
       % V_hv_aftertap(i) = V_hv(i) + V_hv(i).*(-0.025);
        N1 = (13.8 + 13.8*(-0.025))/0.48;
        V_lv_aftertap(i) = V_hv(i)/N1;
        V_drop(i) = V_lv_aftertap(i).*Zint(i).*loading(i);
        V_lv_aftertap1(i) = V_lv_aftertap(i)-V_drop(i);
end

for i = 3:6
        %V_hv_aftertap(i) = V_hv(i) + V_hv(i).*(-0.05);
        N2 = (13.8 + 13.8*(-0.05))/0.48;
        V_lv_aftertap(i) = V_hv(i)/N2;
        V_drop(i) = V_lv_aftertap(i).*Zint(i).*loading(i);
        V_lv_aftertap1(i) = V_lv_aftertap(i)-V_drop(i);
   
end
fprintf("Sec Vtg before tap \n"); disp(abs(V_lv_beforetap));
fprintf("Sec Vtg after tap without vtg drop \n");disp(abs(V_lv_aftertap));
fprintf("Sec Vtg after tap considering vtg drop \n");disp(abs(V_lv_aftertap1));
fprintf("Vtg drop \n");disp(abs(V_drop));

subplot(2,2,2);
plot(Distance, abs(V_lv_beforetap));
grid on
xlabel('Distance (miles)')
ylabel('Voltage (V)')
title('Voltage vs Distance')
hold on;
plot(Distance, abs(V_lv_aftertap1));


%------------------Losses-------------------------------

Loss = conj(Iline) .* Iline .* Z;
Loss = Loss*Sbase;

fprintf("The losses are \n");disp(Loss)
fprintf("The apparent power losses are \n"); disp(abs(Loss))

%************************************************************************
%----------------------------- Capacitor Bank ---------------------------
%************************************************************************
fprintf("**************************************************************** \n");
fprintf("Adding Capacitor Bank \n");
fprintf("**************************************************************** \n");
S = [700 1000 500 1000 1000 1000];    % in kVa
pf = [0.8 0.82 0.85 0.8 0.85 0.8];
loading = [0.8 0.85 0.7 0.9 0.85 0.8];
Vold = [13.8 13.8 13.8 13.8 13.8 13.8];  % in kV

Z_permile = [0.10+0.25i 0.10+0.25i 0.12+0.35i 0.12+0.35i 0.15+0.45i 0.15+0.45i 0.10+0.25i]; % in ohm/mile

Length = [2 3 2.5 4 1.5 3 2]; % in mile
Distance = [2 5 7.5 11.5 13 16];



Z = Z_permile.*Length;
S = S.*loading;
P = S.*pf;
Q = sqrt((S.*S)-(P.*P));
Q(5) = Q(5) - 750;   % Adding the capacitor bank at bus 5



Sbase = 1000; %kVA
Vbase = 13.8; %kV
Zbase = 13800*13800/1000000;
Vregulated = 1;  %kV
P = P./Sbase;
Q = Q./Sbase;
Vold = Vold./Vbase;
Vnew = ones(1,6);
Z = Z./Zbase;
Iline = zeros(1,7);
S = complex(P,Q);

DeltaV =1000;

V_tolerance = 0.01; % in V
V_tolerance = V_tolerance / (Vbase*1000);
%-----------------------Stopping Criterion-------------------------------
Count = 0;
while max(abs(DeltaV)) >= V_tolerance 
    % Backward Sweep
    Iload = conj(S(:)./Vnew(:));
    for i=6:-1:1
        Iline(i) = Iload(i) + Iline(i+1);
    end
    
    % Forward Sweep
    for i=1:6
        if i==1
            Vnew(i) = Vregulated - Iline(i).*Z(i);
        else 
            Vnew(i) = Vnew(i-1) - Iline(i).*Z(i);
        end
    end
    DeltaV = abs(Vold(:) - Vnew(:));
    Vold(:) = Vnew(:);
    Count = Count + 1;
    
end
fprintf("Primary Voltage after adding Capacitor Bank \n");
disp(abs(Vnew));
disp(abs(Vnew)*13.8);
disp(Count)

subplot(2,2,3);
plot(Distance,abs(Vnew));
grid on
xlabel('Distance')
ylabel('Voltage')
title('Voltage vs Distance')
%---------------------------Tap Setting----------------------------------

N = 13.8/0.48; %turns ratio
V_hv = Vnew*Vbase*1000;
Zint = [0.05 0.0575 0.0475 0.0575 0.0575 0.0575];
V_lv = V_hv./N ;
V_lv = V_lv-(V_lv.*Zint.*loading);
V_lv_beforetap = V_lv;

Tap = (abs(V_hv) - 13800)./(13800);
Tap = Tap.*100;

fprintf("The calculated tap setting after adding capacitor banks are \n"); disp(Tap)

% apply 0% tap at T1, -2.5% tap at T2, T3 and -5% tap at T4, T5, T6

N1 = 0;
N2 = 0;
V_lv_aftertap = zeros(1,6);
V_lv_aftertap1 = zeros(1,6);



for i = 1:3

       if i ==1
           N1 = (13.8)/0.48;
           V_lv_aftertap(i) = V_hv(i)/N1; 
           V_drop(i) = V_lv_aftertap(i).*Zint(i).*loading(i);
           V_lv_aftertap1(i) = V_lv_aftertap(i)-V_drop(i);
       else
       % V_hv_aftertap(i) = V_hv(i) + V_hv(i).*(-0.025);
        N1 = (13.8 + 13.8*(-0.025))/0.48;
        V_lv_aftertap(i) = V_hv(i)/N1; 
        V_drop(i) = V_lv_aftertap(i).*Zint(i).*loading(i);
        V_lv_aftertap1(i) = V_lv_aftertap(i)-V_drop(i);
       end
end
for i = 4:6

        %V_hv_aftertap(i) = V_hv(i) + V_hv(i).*(-0.05);
        N2 = (13.8 + 13.8*(-0.05))/0.48;
        V_lv_aftertap(i) = V_hv(i)/N2;
        V_drop(i) = V_lv_aftertap(i).*Zint(i).*loading(i);
        V_lv_aftertap1(i) = V_lv_aftertap(i)-V_drop(i);
       
end
fprintf("Sec Vtg before tap \n"); disp(abs(V_lv_beforetap));
fprintf("Sec Vtg after tap without vtg drop \n");disp(abs(V_lv_aftertap));
fprintf("Sec Vtg after tap considering vtg drop \n");disp(abs(V_lv_aftertap1));

subplot(2,2,4);
plot(Distance, abs(V_lv_beforetap));
grid on
xlabel('Distance (miles)')
ylabel('Voltage (V)')
title('Voltage vs Distance')
hold on;
plot(Distance, abs(V_lv_aftertap1));
fprintf("vtg drop after adding capacitor \n"); disp(abs(V_drop));


%------------------Losses-------------------------------

Loss = conj(Iline) .* Iline .* Z;
Loss = Loss.*Sbase;

fprintf("The losses after adding capacitor bank are \n");disp(Loss)
fprintf("The apparent power losses adding capacitor bank are \n"); disp(abs(Loss))
